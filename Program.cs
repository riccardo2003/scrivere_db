﻿using System;
using System.Collections.Generic;
using System.IO;
using MySql.Data.MySqlClient;
using CsvHelper;
namespace srivereDB
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            string cs=@"server=localhost; userid=user1; password=Pippo2003; database=analisi_programmazione_film;";
            using var con=new MySqlConnection(cs);
            con.Open();
            Console.WriteLine($"mysqlversion:{con.ServerVersion}");

            
            
            /*using var cmd=new MySqlCommand();
            cmd.Connection=con;
            cmd.CommandText="INSERT INTO movie (titolo_originale, titolo_italiano, anno_produzione, regia_id, durata, paese_produzione, distribuzione) VALUES ('Star Wars', 'Guerre Stellari', '1977', '1', '121', 'USA', '20th Century Fox')";
            cmd.CommandText="INSERT INTO movie (titolo_originale, titolo_italiano, anno_produzione, regia_id, durata, paese_produzione, distribuzione) VALUES ('Creed', 'Creed', '2013', '2', '133', 'USA', 'Warner Bros')";
            cmd.CommandText="INSERT INTO movie (titolo_originale, titolo_italiano, anno_produzione, regia_id, durata, paese_produzione, distribuzione) VALUES ('Rocky', 'Rocky', '1976', '3', '119', 'USA', 'United Artists, Chartoff-Winkler Productions')";
            cmd.CommandText="INSERT INTO movie (titolo_originale, titolo_italiano, anno_produzione, regia_id, durata, paese_produzione, distribuzione) VALUES ('Harry Potter', 'Harry Potter', '2000', '4', '152', 'UK', 'Warner Bros')";
            cmd.CommandText="INSERT INTO movie (titolo_originale, titolo_italiano, anno_produzione, regia_id, durata, paese_produzione, distribuzione) VALUES ('Beetlejuice', 'Beetlejuice', '1988', '5', '92', 'USA', 'Warner Bros')";
            cmd.CommandText="INSERT INTO movie (titolo_originale, titolo_italiano, anno_produzione, regia_id, durata, paese_produzione, distribuzione) VALUES ('Scarface', 'Scarface', '1983', '6', '170', 'USA', 'Universal Pictures')";
            cmd.ExecuteNonQuery();*/
            // var Sql="INSERT INTO movie (titolo_originale, titolo_italiano, anno_produzione, regia_id, durata, paese_produzione, distribuzione) VALUES (@titolo_originale, @titolo_italiano, @anno_produzione, @regia_id, @durata, @paese_produzione, @distribuzione)";
            // using var cmd=new MySqlCommand(sql,con);
            // cmd.Parameters.AddWidthValue("@titolo_originale", "Harry Potter");
            // cmd.Parameters.AddWidthValue("@titolo_italiano", "Harry Potter");
            // cmd.Parameters.AddWidthValue("@anno_produzione", "2000");
            // cmd.Parameters.AddWidthValue("@regia_id", "4");
            // cmd.Parameters.AddWidthValue("@durata", "152");
            // cmd.Parameters.AddWidthValue("@paese_produzione", "UK");
            // cmd.Parameters.AddWidthValue("@distribuzione", "Warner Bros");
            // cmd.Prepare();
            // try
            // {cmd.ExecuteNonQuery();
            // //Console.WriteLine("la riga è stata inserita");}
            // catch (Exception e)
            // {
            //     Console.WriteLine("errore"+e.ToString());}

                /*string[,] matrice = { { "Star Wars", "Guerre Stellari", "1977", "1", "121", "USA", "20th Century Fox" }, { "Creed", "Creed", "2013", "2", "133", "USA", "Warner Bros" }, { "Rocky", "Rocky", "1976", "3", "119", "USA", "United Artists, Chartoff-Winkler Productions" }, { "Harry Potter", "Harry Potter", "2000", "4", "152", "UK", "Warner Bros" }, {"Batman begins", "Batman begins","2005", "7", "140","USA","Warner Bros"} };
                
            for (int j=0;j<5;j++)
             {
                var sql="INSERT INTO movie(titolo_originale,titolo_italiano,anno_produzione,regia_id,durata,paese_produzione,distribuzione)VALUES(@titolo_originale,@titolo_italiano,@anno_produzione,@regia_id,@durata,@paese_produzione,@distribuzione)";
                using var cmd= new MySqlCommand(sql,con);
                cmd.Parameters.AddWithValue("@titolo_originale",matrice[j,0]);
                cmd.Parameters.AddWithValue("@titolo_italiano",matrice[j,1]);
                cmd.Parameters.AddWithValue("@anno_produzione",matrice[j,2]);
                cmd.Parameters.AddWithValue("@regia_id",matrice[j,3]);
                cmd.Parameters.AddWithValue("@durata",matrice[j,4]);
                cmd.Parameters.AddWithValue("@paese_produzione",matrice[j,5]);
                cmd.Parameters.AddWithValue("@distribuzione",matrice[j,6]);
                cmd.Prepare();
            try{
            cmd.ExecuteNonQuery();
            Console.WriteLine("La riga è stata inserita");
            }
            catch(Exception e)
            {
                Console.WriteLine("Errore "+ e.ToString());
            }            
            }*/



            TextReader reader1 = new StreamReader("attori.csv");
            var csvReader1 = new CsvReader(reader1, System.Globalization.CultureInfo.CurrentCulture);
            var actorsfromcsv = csvReader1.GetRecords<Actor>();
            var Sql="INSERT INTO actors (name, surname, year, fiscalcode, role) VALUES (@name, @surname, @year, @fiscalcode, @role)";
            using var cmd= new MySqlCommand(Sql,con);
            foreach (Actor actorfromcsv in actorsfromcsv)
            {

                Console.WriteLine(" " + actorfromcsv.Name + " " + actorfromcsv.Surname);
                // Console.WriteLine(" " + actorfromcsv.Surname);
                }
           /*     cmd.Parameters.AddWithValue("@name",matrice[j,0]);
                cmd.Parameters.AddWithValue("@surname",matrice[j,1]);
                cmd.Parameters.AddWithValue("@year",matrice[j,2]);
                cmd.Parameters.AddWithValue("@fiscalcode",matrice[j,3]);
                cmd.Parameters.AddWithValue("@role",matrice[j,4]);
                cmd.Prepare();*/
        }
    }
    class Person
    {
        private string _indirizzo;        //  public string Name { get; set; }
        private string _name;
        public string Name
        {
            get => _name;
            set => _name = value;
        }        public string Surname { get; set; }
        public int Year { get; set; }        private string _fiscalcode;  // the name field
        private int _eta;  // the name field        
        public string FiscalCode    // the Name property
        {
            get => _fiscalcode;
            set => _fiscalcode = value;
        }
        public string StampaMessaggio(string messaggino)
        {
            //questa è una virtual
            return "questo è il metodo della persona" + messaggino;
        }
        protected int Age()    // the Name property
        {
            //get => _fiscalcode;
            return (2021 - (Year));
        }
        public string nomeCompleto()
        {
            return Name + "  " + Surname + " " + _fiscalcode + " " + Age();
        }
    }
    class Actor : Person
    {
        public string Role { get; set; }        public string rolePlaying()
        {
            return "the actor's name is " + Name + "  " + Surname + " " + Role + Age();
        }
        enum ActorType
        {
            None,
            Protagonist,
            Coprotagonist,
            VoiceActor
        }
        /* public override string StampaMessaggio()
         {
             //c'è un override
             return "questo è il metodo dell'attore ";
         }*/
    }
}
